jQuery(document).ready(function($) {

  // banner home
  $(document).ready(function(){
    $('.bxslider').bxSlider({
      pagerCustom: '#bx-pager',
      controls: false,
      auto: true,
      autoHover: true
    });
    $('.bxslider2').bxSlider({
      pagerCustom: '#bx-pager2',  
      controls: false,
      auto: true,
      autoHover: true
    });
  });

  // btn expandir todos os servicos
  $('.todos-servicos').on('click', function(){
    if($('.servicos').hasClass('show')){
      $('.servicos').removeClass('show');
    } else {
      $('.servicos').addClass('show');
    }
  });


  // carousel
  $(document).ready(function() {
    $('.owl-carousel').owlCarousel({
      items: 1,
      dots:false,
      loop: false,
      center: true,
      margin: 10,
      callbacks: true,
      URLhashListener: true,
      autoplayTimeout:1000,
      autoplayHoverPause:true,
      animateOut: 'fadeOut',
      startPosition: 'URLHash'
    });
  })

  // scroll to
  function goToByScroll(id){
    // Remove "link" from the ID
    id = id.replace("link", ""),
    headerH = $('.header-top').height();

    // Scroll
    $('html,body').animate({
        scrollTop: ($("#"+id).offset().top-headerH-40)},
        'slow');
    }

    $("a.scrollto ").click(function(e) {
          // Prevent a page reload when a link is pressed
        e.preventDefault();
          // Call the scroll function
        goToByScroll(this.id);
    });

    // abrir modal
    function abrirModal(modal){
      $(modal).addClass('ativo');
      $(modal).find('.btnFechar').on('click', function() {
        $(modal).removeClass('ativo');
      });
    }

    $('.autoAnual').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-auto-anual');
    });

    $('.autoSemestral').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-auto-semestral');
    });

    $('.motoAnual').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-moto-anual');
    });

    $('.motoSemestral').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-moto-semestral');
    });

    $('.caminhaoAnual').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-caminhao-anual');
    });

    $('.caminhaoSemestral').on('click', function(event) {
      event.preventDefault();

      abrirModal('.modal-caminhao-semestral');
    });


    // menu

    $( window ).scroll(function() {
      if($('.home').scrollTop()>20){
        $('.navbar').addClass('active');
      }
      if($('.home').scrollTop()<=20){
        $('.navbar').removeClass('active');
      }
    });

    // banner
    $(".pontos .p").on("mouseover", function(event){
      event.preventDefault();
      var mask = $(this).data('mask');
      $(mask).addClass('active');
    });
    $(".pontos .p").on("mouseout", function(event){
      event.preventDefault();
      var mask = $(this).data('mask');
      $(mask).removeClass('active');
    });

});

// Back to Top ===============================================
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');
// /Back to Top ===============================================
