// formatar inputs
function formatar(mascara, documento){
  var i = documento.value.length;
  var saida = mascara.substring(0,1);
  var texto = mascara.substring(i)

  if (texto.substring(0,1) != saida){
            documento.value += texto.substring(0,1);
  }

}

jQuery(document).ready(function($) {
  var etapa2pos = $('.etapa-2').offset();

  // function animation scroll
  function aniScroll(el){
    var pos = $(el).offset();
    $("html, body").animate({ scrollTop: (pos.top - 130) }, 600);
  };

  // btn
  $('.btn-etapa-1').on('click', function(){
    $('.etapa-2').addClass('active');
    aniScroll('.etapa-2');
    $(this).css({display:'none'});
  });
  $('.btn-etapa-2').on('click', function(){
    $('.etapa-3').addClass('active');
    aniScroll('.etapa-3');
    $(this).css({display:'none'});
  });
  $('.btn-etapa-3').on('click', function(){
    $('.etapa-4').addClass('active'); 
    aniScroll('.etapa-4');
    $(this).css({display:'none'});
  });
  $('.btn-etapa-4').on('click', function(){
    $('.etapa-5').addClass('active');
    aniScroll('.etapa-5');
    var ok = setInterval(function(){
      $('.etapa-6').addClass('active');
      aniScroll('.etapa-6');
      clearInterval(ok);
    },1000);
  });
});
